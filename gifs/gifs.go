package gifs

import (
	"context"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"hash/crc32"
	"io"
	"mime/multipart"
	"path/filepath"
	"time"

	"github.com/pkg/errors"
)

var crc32cTable = crc32.MakeTable(crc32.Castagnoli)

type GIFsService struct {
	client *Client
}

type GIF struct {
	ID         string    `json:"id"`
	Collection string    `json:"collectionID"`
	Path       string    `json:"path"`
	FileHash   string    `json:"hash"`
	CreatedAt  time.Time `json:"createdAt,omitempty"`
	CreatedBy  string    `json:"createdBy"`
}

type GIFChange struct {
}

type GIFsFilter struct {
}

func (g GIFsService) Upload(ctx context.Context, collection, path string, r io.ReadSeeker) (GIF, error) {
	// generate our CRC32c hash
	crc := crc32.New(crc32cTable)
	if _, err := io.Copy(crc, r); err != nil {
		return GIF{}, errors.Wrap(err, "error generating CRC32c hash")
	}
	crc32c := base64.StdEncoding.EncodeToString(crc.Sum(nil))

	// reset the input to the beginning
	_, err := r.Seek(0, io.SeekStart)
	if err != nil {
		return GIF{}, errors.Wrap(err, "error resetting reader after CRC32c")
	}

	// generate our SHA256 hash
	sha := sha256.New()
	if _, err := io.Copy(sha, r); err != nil {
		return GIF{}, errors.Wrap(err, "error generating SHA256 hash")
	}
	shaHash := hex.EncodeToString(sha.Sum(nil))

	// reset the input to the beginning
	_, err = r.Seek(0, io.SeekStart)
	if err != nil {
		return GIF{}, errors.Wrap(err, "error resetting reader after SHA256")
	}

	// stream data from the multipart writer into the request body
	mpReader, mpWriter := io.Pipe()

	headerChan := make(chan string)

	go func(path, sha, crc string, r io.Reader, w *io.PipeWriter, ctChan chan string) {
		// if our reader can be closed, close it when we're done here
		if rc, ok := r.(io.ReadCloser); ok {
			defer rc.Close()
		}
		writer := multipart.NewWriter(w)
		ctChan <- writer.FormDataContentType()
		err := writer.WriteField("path", path)
		if err != nil {
			w.CloseWithError(errors.Wrap(err, "error writing path"))
			return
		}
		err = writer.WriteField("sha256", sha)
		if err != nil {
			w.CloseWithError(errors.Wrap(err, "error writing sha256"))
			return
		}
		err = writer.WriteField("crc32c", crc)
		if err != nil {
			w.CloseWithError(errors.Wrap(err, "error writing crc32c"))
			return
		}

		// upload the reader last
		part, err := writer.CreateFormFile("gif", filepath.Base(path))
		if err != nil {
			w.CloseWithError(errors.Wrap(err, "error creating form file"))
			return
		}
		if _, err = io.Copy(part, r); err != nil {
			w.CloseWithError(errors.Wrap(err, "error uploading file"))
			return
		}

		err = writer.Close()
		if err != nil {
			w.CloseWithError(errors.Wrap(err, "error closing multipart writer"))
			return
		}
		w.Close()
	}(path, shaHash, crc32c, r, mpWriter, headerChan)

	req, err := g.client.BaseRequest("POST", fmt.Sprintf("/v1/collections/%s/upload", collection), mpReader)
	if err != nil {
		return GIF{}, err
	}

	contentType := <-headerChan
	req.Header.Set("Content-Type", contentType)

	resp, err := g.client.doer.Do(req)
	if err != nil {
		return GIF{}, err
	}
	result, err := g.client.GetResponse(ctx, resp.Body)
	if err != nil {
		return GIF{}, err
	}
	// TODO: better error handling
	if len(result.GIFs) > 0 {
		return result.GIFs[0], nil
	}
	return GIF{}, nil
}

func (g GIFsService) Alias(ctx context.Context, from, to string) (GIF, error) {
	return GIF{}, nil
}

func (g GIFsService) Get(ctx context.Context, id string) (GIF, error) {
	return GIF{}, nil
}

func (g GIFsService) GetByURL(ctx context.Context, u string) (GIF, error) {
	return GIF{}, nil
}

func (g GIFsService) Download(ctx context.Context, id string) (io.Reader, error) {
	return nil, nil
}

func (g GIFsService) Update(ctx context.Context, id string, change GIFChange) error {
	return nil
}

func (g GIFsService) Delete(ctx context.Context, id string) error {
	return nil
}

func (g GIFsService) List(ctx context.Context, filter GIFsFilter) ([]GIF, error) {
	return nil, nil
}

func (g GIFsService) Reprocess(ctx context.Context, id string) error {
	return nil
}
