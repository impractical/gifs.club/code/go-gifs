package gifs

import (
	"context"
	"encoding/json"
	"io"
	"io/ioutil"

	"darlinggo.co/api"
)

type Response struct {
	GIFs        []GIF              `json:"gifs,omitempty"`
	Collections []Collection       `json:"collections,omitempty"`
	Errors      []api.RequestError `json:"errors,omitempty"`
}

func (c *Client) GetResponse(ctx context.Context, r io.Reader) (Response, error) {
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return Response{}, err
	}
	if rc, ok := r.(io.ReadCloser); ok {
		err = rc.Close()
	}
	var resp Response
	err = json.Unmarshal(b, &resp)
	if err != nil {
		return Response{}, err
	}
	return resp, nil
}
