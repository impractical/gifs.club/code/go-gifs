package gifs

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"strings"

	"darlinggo.co/version"
)

type Client struct {
	doer    doer
	baseURL *url.URL
}

func NewClient(doer doer, baseURL *url.URL) *Client {
	return &Client{doer: doer, baseURL: baseURL}
}

func (c *Client) BaseRequest(method, path string, body io.Reader) (*http.Request, error) {
	u, err := url.Parse(path)
	if err != nil {
		return nil, err
	}
	baseU := *c.baseURL
	baseU.Path = strings.TrimSuffix(baseU.Path, "/") + "/" + strings.TrimPrefix(u.Path, "/")
	q := baseU.Query()
	for k, v := range u.Query() {
		q[k] = append(q[k], v...)
	}
	baseU.RawQuery = q.Encode()
	req, err := http.NewRequest(method, baseU.String(), body)
	if err != nil {
		return nil, err
	}
	version := version.Tag
	if version != "" {
		version = " (" + version + ")"
	}
	req.Header.Set("User-Agent", "gifs.club/code/go-gifs/gifs"+version)
	return req, nil
}

func (c *Client) JSONRequest(method, path string, body interface{}) (*http.Request, error) {
	b, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}
	req, err := c.BaseRequest(method, path, bytes.NewBuffer(b))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	return req, nil
}

func (c *Client) Collections() CollectionsService {
	return CollectionsService{client: c}
}

func (c *Client) GIFs() GIFsService {
	return GIFsService{client: c}
}

type doer interface {
	Do(r *http.Request) (*http.Response, error)
}
