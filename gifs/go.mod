module gifs.club/code/go-gifs/gifs

require (
	bitbucket.org/ww/goautoneg v0.0.0-20120707110453-75cd24fc2f2c // indirect
	darlinggo.co/api v0.0.0-20160924005218-06eb95038fc2
	darlinggo.co/version v1.0.0
	github.com/google/uuid v1.0.0 // indirect
	github.com/pborman/uuid v0.0.0-20180906182336-adf5a7427709 // indirect
	github.com/pkg/errors v0.8.0
)
