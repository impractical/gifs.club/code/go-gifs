package gifs

import (
	"context"
	"fmt"
	"net/url"
	"strings"
	"time"

	"github.com/pkg/errors"
)

var (
	ErrCollectionNotFound = errors.New("collection not found")
)

type CollectionsService struct {
	client *Client
}

type Collection struct {
	ID           string    `json:"id"`
	Slug         string    `json:"slug"`
	Name         string    `json:"name"`
	Description  string    `json:"description"`
	CreatedAt    time.Time `json:"createdAt"`
	LastUploadAt time.Time `json:"lastUploadAt"`
	Owners       []string  `json:"ownerIDs"`
}

type CollectionChange struct {
}

type CollectionsFilter struct {
}

func (c CollectionsService) Get(ctx context.Context, id string) (Collection, error) {
	req, err := c.client.BaseRequest("GET", fmt.Sprintf("/v1/collections/%s", id), nil)
	if err != nil {
		return Collection{}, err
	}
	resp, err := c.client.doer.Do(req)
	if err != nil {
		return Collection{}, err
	}
	result, err := c.client.GetResponse(ctx, resp.Body)
	if err != nil {
		return Collection{}, err
	}
	for _, collection := range result.Collections {
		if strings.ToLower(collection.ID) == strings.ToLower(id) {
			return collection, nil
		}
	}
	// TODO: better error handling
	return Collection{}, ErrCollectionNotFound
}

func (c CollectionsService) GetByDomain(ctx context.Context, domain string) (Collection, error) {
	req, err := c.client.BaseRequest("GET", fmt.Sprintf("/v1/collections/?%s", url.Values{"slug": []string{domain}}.Encode()), nil)
	if err != nil {
		return Collection{}, err
	}
	resp, err := c.client.doer.Do(req)
	if err != nil {
		return Collection{}, err
	}
	result, err := c.client.GetResponse(ctx, resp.Body)
	if err != nil {
		return Collection{}, err
	}
	for _, collection := range result.Collections {
		if strings.ToLower(collection.Slug) == strings.ToLower(domain) {
			return collection, nil
		}
	}
	// TODO: better error handling
	return Collection{}, ErrCollectionNotFound
}

func (c CollectionsService) Create(ctx context.Context, collection Collection) error {
	req, err := c.client.JSONRequest("POST", "/v1/collections", collection)
	if err != nil {
		return errors.Wrap(err, "building request")
	}
	resp, err := c.client.doer.Do(req)
	if err != nil {
		return errors.Wrap(err, "sending request")
	}
	result, err := c.client.GetResponse(ctx, resp.Body)
	if err != nil {
		return errors.Wrap(err, "reading response")
	}
	if len(result.Errors) > 0 {
		// TODO: return error
	}
	return nil
}

func (c CollectionsService) Update(ctx context.Context, id string, change CollectionChange) error {
	return nil
}

func (c CollectionsService) Delete(ctx context.Context, id string) error {
	return nil
}

func (c CollectionsService) List(ctx context.Context, filter CollectionsFilter) ([]Collection, error) {
	return nil, nil
}
